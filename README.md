# MocapROS

Mocap ros package to create an easy to use ros package to integrate Mocap. 
The Mocap (Motion Capture System) is based on a [Motive Optitrack System](https://optitrack.com/software/motive/).
The camera system can track the infra reflectice markers to track defined markers and rigid bodies in the 
observed space. This ros package integrate the system into ROS and makes a `/stream` topic to publis the data.

Motive version: 2.3 <br>
NatNetSDK version: 3.1 (old version in previous code: 2.10)

**Tested with:**
- Ubuntu 20.04 <br>
- ROS Noetic

## Install 

    $ cd ~/catkin_ws/src
    $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/mocapros.git
    $ cd ~/catkin_ws
    $ catkin_make

## Bringup

The client should be connected to the **Mozgaslabor** wifi. <br>
The local ip is updated automaticaly.

wifi_id:  **Mozgaslabor** <br>
password: **csaktudomanyoscelrahasznalom** <br> 

    $ rosrun mocapstream stream_publisher.py

Listen to the stream:

    $ rostopic echo /stream

## Data format:
  - data: **StreamVector** - Multi array with **RigidBody**
    - name: **String**
    - id: **int32**
    - pos: **PosVector**
      - x: **float32**
      - y: **float32**
      - z: **float32**
    - orientation: **OriVector**
      - euler: **EulerVector**
        - roll: **float32**
        - pitch: **float32**
        - yaw: **float32**
      - quat: **QuatVector**
        - qx: **float32**
        - qy: **float32**
        - qz: **float32**
        - qw: **float32**

## References

[NatNetSDK](https://optitrack.com/support/downloads/developer-tools.html#natnet-sdk)

## Authors and acknowledgment
Balázs Nagy (email: nagybalazs@mogi.bme.hu) <br>
Bálint Hegedűs <br>

## License
The project is open source under the lincense of BSD.

## TODO:

- Tracked - Not Tracked value