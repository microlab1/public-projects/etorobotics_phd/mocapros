'''
Ez a kód az eredeti irányvektort használva quaternió szorzással kiszámolja az új irányvetort
normálja azt
majd a két vektort az XZ síkra vetíti
és kiszámolja a szögeit.
A vektor megfelelő térfélbe mutatásával kiegészítí a - szögeket

Ezzel a számolással helyettesítve a YAW szöget

PITCH és ROLLra is meg kéne csinálni és feltölteni vele az EULER szögeket.
'''

import numpy as np
from math import degrees
np.seterr(invalid='ignore')

def Quat2Euler(qw, qx, qy, qz):
    R = np.array([qw, qx, qy, qz])  # R  = [w,  x,  y,  z]
    Rt = np.array([qw, -qx, -qy, -qz])  # R' = [w, -x, -y, -z]

    Px  = np.array([0, 1, 0, 0])
    Py = np.array([0, 0, 1, 0])
    #Pz = np.array([0, 0, 0, 1])

    Ptx = H(H(R, Px), Rt)
    Pty = H(H(R, Py), Rt)
    #Ptz = H(H(R, Pz), Rt)

    yaw   = projectVector2D_yaw(Px, Ptx)
    pitch = projectVector2D_pitch(Px, Ptx)
    roll  = projectVector2D_roll(Py, Pty)

    return [yaw, pitch, roll]

def H(quaternion1, quaternion0):
    w0, x0, y0, z0 = quaternion0
    w1, x1, y1, z1 = quaternion1
    return np.array([-x1 * x0 - y1 * y0 - z1 * z0 + w1 * w0,
                             x1 * w0 + y1 * z0 - z1 * y0 + w1 * x0,
                             -x1 * z0 + y1 * w0 + z1 * x0 + w1 * y0,
                             x1 * y0 - y1 * x0 + z1 * w0 + w1 * z0], dtype=np.float64)

def projectVector2D_yaw(P, Pt):
    vector_1 = [P[1], P[3]]
    vector_2 = [Pt[1], Pt[3]]

    unit_vector_1 = vector_1 / np.linalg.norm(vector_1)
    unit_vector_2 = vector_2 / np.linalg.norm(vector_2)
    dot_product = np.dot(unit_vector_1, unit_vector_2)
    angle_rad = np.arccos(dot_product)

    angle_deg = degrees(angle_rad)
    if Pt[3] > 0:
        angle_deg = -angle_deg

    return angle_deg


def projectVector2D_pitch(P, Pt):
    vector_1 = [P[1], P[2]]
    vector_2 = [Pt[1], Pt[2]]

    unit_vector_1 = vector_1 / np.linalg.norm(vector_1)
    unit_vector_2 = vector_2 / np.linalg.norm(vector_2)
    dot_product = np.dot(unit_vector_1, unit_vector_2)
    angle_rad = np.arccos(dot_product)

    angle_deg = degrees(angle_rad)
    if Pt[2] < 0:
        angle_deg = -angle_deg

    return angle_deg

def projectVector2D_roll(P, Pt):
    vector_1 = [P[2], P[3]]
    vector_2 = [Pt[2], Pt[3]]

    unit_vector_1 = vector_1 / np.linalg.norm(vector_1)
    unit_vector_2 = vector_2 / np.linalg.norm(vector_2)
    dot_product = np.dot(unit_vector_1, unit_vector_2)
    angle_rad = np.arccos(dot_product)

    angle_deg = degrees(angle_rad)
    if Pt[3] > 0:
        angle_deg = -angle_deg

    return -angle_deg