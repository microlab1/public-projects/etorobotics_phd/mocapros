#!/usr/bin/env python3

import rospy
from mocapy import *
from mocapstream.msg import StreamVector
from mocapstream.msg import RigidBody
import socket
from quat_angle import *
import copy

def publisher(mocap):
    while not rospy.is_shutdown():
        streamvector = StreamVector()
        rigidbody = RigidBody()
        streamed_bodies = []

        while not mocap.queue.empty():
            frame = mocap.queue.get(timeout=1)

            for body in frame.rigid_bodies:
                # Name
                rigidbody.name = body.name
                # ID
                rigidbody.id = body.id
                rigidbody.tracked = body.tracked
                # POS
                rigidbody.pos.x = body.position.x
                rigidbody.pos.y = body.position.y
                rigidbody.pos.z = body.position.z
                # Orientation
                rigidbody.orientation.quat.qx = body.rotation.x
                rigidbody.orientation.quat.qy = body.rotation.y
                rigidbody.orientation.quat.qz = body.rotation.z
                rigidbody.orientation.quat.qw = body.rotation.w

                rigidbody.orientation.euler.yaw   = Quat2Euler(body.rotation.w, body.rotation.x, body.rotation.y, body.rotation.z)[0] # OK
                rigidbody.orientation.euler.pitch = Quat2Euler(body.rotation.w, body.rotation.x, body.rotation.y, body.rotation.z)[1] # not OK
                rigidbody.orientation.euler.roll  = Quat2Euler(body.rotation.w, body.rotation.x, body.rotation.y, body.rotation.z)[2] # not OK

                # Append rigid body to data
                if rigidbody.name not in streamed_bodies:
                    streamvector.data.append(copy.deepcopy(rigidbody))
                    streamed_bodies.append(rigidbody.name)
                else:
                    i = streamed_bodies.index(rigidbody.name)
                    streamvector.data[i] = rigidbody

        pub.publish(streamvector)
        rate.sleep()

if __name__ == "__main__":
    # Init node and topic
    rospy.init_node('Mocap_node')
    pub = rospy.Publisher('stream', StreamVector, queue_size=5)
    rate = rospy.Rate(20)

    # Get local IP address
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 1))  # connect() for UDP doesn't send packets
    local_ip = s.getsockname()[0]

    # Init mocap and start
    mocap = MoCap(server_ip_address='192.168.100.105', local_ip_address=local_ip)
    mocap.natnet_start()

    rospy.loginfo('Mocapy node initialized')
    rospy.loginfo('Local_ip: %s', local_ip)
    rospy.loginfo('Start streaming')

    # Main function
    publisher(mocap)

    # Shut down
    rospy.loginfo("Shutting down")
    mocap.natnet_stop()