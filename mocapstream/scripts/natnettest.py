#!/usr/bin/env python3
"""
This python code can test the stream without ROS.

Local_ip_address needs to be updated accordingly!
"""

from mocapy import MoCap
import time

mc = MoCap(server_ip_address="192.168.100.105", local_ip_address="192.168.100.156")
mc.natnet_start()

print("Mocap Started")

from queue import Queue

q = Queue()

class BodyInfo:
    def __init__(self, name, id):
        self.name = name
        self.id = id
    def setpos(self,x,y,z):
        self.pos = [x,y,z]
    def setori(self,x,y,z,zs):
        self.ori = [x,y,z,zs]




def convertToRosFormat():
    lasttime=time.time()
    while True:

        frame = mc.queue.get(timeout=1.0)
        for body in frame.rigid_bodies:
            b = BodyInfo(body.name, body.id)
            b.setpos(body.position.x, body.position.y, body.position.z)
            b.setori(body.rotation.x, body.rotation.y, body.rotation.z, body.rotation.w)
            q.put(b)


        print(time.time()-lasttime)
        lasttime = time.time()

while True:
    convertToRosFormat()
